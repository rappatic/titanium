### Fonts

Titanium has built-in support for 7 fonts:

![Lora](https://assets.awwwards.com/awards/gallery/2018/05/google-fonts-collection-awwwards9.jpg)
![Archivo](https://assets.awwwards.com/awards/gallery/2018/05/google-fonts-collection-awwwards.jpg)
![Montserrat](https://assets.awwwards.com/awards/gallery/2018/05/google-fonts-collection-awwwards3.jpg)
![Lato](https://assets.awwwards.com/awards/gallery/2018/05/google-fonts-collection-awwwards2-1.jpg)
![Poppins](https://assets.awwwards.com/awards/gallery/2018/05/google-fonts-collection-awwwards29.jpg)
![Slabo](https://preview.ibb.co/f2k2OK/Slabo_1.png)
![Crimson Text](https://preview.ibb.co/jfydxe/Slabo_2.png)

To use a font, add the class `font-` followed by the first word of the font name, lowercase and without spaces. For example, if you wished to use the font Crimson Text, you would add the class `font-crimson`.
