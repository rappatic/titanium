

  <p align="center">
    <img src="https://preview.ibb.co/hdpYze/Picture3.png" alt="I am TI-TAN-I-UM">
  <br>
  <br>
  <br>
    <a href="https://rappatic.github.io/titanium/documentation.html#/"><img src="https://preview.ibb.co/kFFyze/Picture12.png"></a><br>
    <a href="https://rappatic.github.io/titanium/"><img src="https://preview.ibb.co/fcxHsz/Picture1.png"></a><br>
    <a href="https://github.com/rappatic/titaniumcss/projects/2"><img src="https://preview.ibb.co/nxJe5K/Picture19.png"></a><br>
    <br>
  <br>
</p>


## Installing

To install Titanium, add the following to the `head` section of your webpage:

`<link rel="stylesheet" href="https://cdn.rawgit.com/rappatic/titanium/master/titanium.css">`

`<script src="https://cdn.rawgit.com/rappatic/titanium/master/titanium/titanium.js"></script>`

And that's all before you're up and running! To learn how to use Titanium, check out the documentation.

<a href="https://rappatic.github.io/titanium/documentation.html#/?id=get-an-older-version-of-titanium">Want to install an old version?</a>

## Reporting Bugs

When you report a bug, make sure that the issue doesn't already exist. Once you've done that, include as much as you can about the bug, how to recreate it, and the device(s) and browser(s) it occurs on. If possible, include where you think the problem is.

[Report a bug or issue](https://github.com/rappatic/titaniumcss/issues)

## Creators

Titanium created by <a href="https://rappatic.com" target="_blank">rappatic</a> in 2018.

